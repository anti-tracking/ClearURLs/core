/*
 * ClearURLs
 * Copyright (c) 2017-2022 Kevin Röbert.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import URLHashParams from '../../src/utils/URLHashParams'

describe('URLHashParams', () => {

    it('should create correct instance', () => {
        const url = new URL('https://clearurls.xyz#foo=bar&importantParam&bar=foo')
        const params = new URLHashParams(url)

        expect(params.toString()).toStrictEqual(url.hash.slice(1))
    })

    it('should create from empty hash', () => {
        const url = new URL('https://clearurls.xyz')
        const params = new URLHashParams(url)

        expect(params.toString()).toStrictEqual('')
    })

    it('should handle multiple values', () => {
        const url = new URL('https://clearurls.xyz#foo=bar&foo=bar2')
        const params = new URLHashParams(url)
        const expected = new Set<string>()

        expected.add('bar')
        expected.add('bar2')

        expect(params.getAll('foo')).toStrictEqual(expected)
        expect(params.get('foo')).toStrictEqual('bar')
    })

    it('should handle empty values', () => {
        const url = new URL('https://clearurls.xyz#foo=bar&importantParam')
        const params = new URLHashParams(url)

        expect(params.get('importantParam')).toBeNull()
    })

    it('should delete params', () => {
        const url = new URL('https://clearurls.xyz#foo=bar&importantParam&bar=foo')
        const params = new URLHashParams(url)

        params.delete('foo')
        params.delete('importantParam')

        expect(params.toString()).toStrictEqual('bar=foo')
    })

    it('should add params', () => {
        const url = new URL('https://clearurls.xyz')
        const params = new URLHashParams(url)

        params.append('foo', 'bar')
        params.append('importantParam')

        expect(params.toString()).toStrictEqual('foo=bar&importantParam')
    })
})
