/*
 * ClearURLs
 * Copyright (c) 2017-2022 Kevin Röbert.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import Preprocessor from '../preprocessors/preprocessor'
import ReplacePattern from '../replacePattern'
import RequestType from '../requestTypes/requestType'

/**
 * Models a rule.
 */
export default class Rule {
    private readonly _matchPattern: RegExp
    private readonly _replacePattern: ReplacePattern
    private readonly _requestTypes: RequestType[]
    private readonly _description: string
    private readonly _preprocessors: Preprocessor[]
    private readonly _exceptions: RegExp[]
    private _active: boolean

    /**
     * Creates a new {@link Rule}.
     *
     * @param matchPattern the pattern this rule must match to for further processing
     * @param replacePattern the pattern to which the match will be replaced, default is {@link ReplacePattern.REMOVE}
     * @param requestTypes the request types to which this rule will be applied, default is {@link RequestType.ALL}
     * @param description the description of this rule
     * @param preprocessors the {@link Preprocessor}'s that are applied before the replace phase
     * @param exceptions the exception when the rule should not be applied
     * @param active if the matchPattern is active or not
     */
    constructor(matchPattern: string, replacePattern: ReplacePattern = ReplacePattern.REMOVE,
                requestTypes: RequestType[] = [RequestType.ALL], description: string = '',
                preprocessors: Preprocessor[] = [], exceptions: RegExp[] = [], active: boolean = true) {
        this._matchPattern = new RegExp(matchPattern)
        this._replacePattern = replacePattern
        this._requestTypes = requestTypes
        this._description = description
        this._preprocessors = preprocessors
        this._exceptions = exceptions
        this._active = active
    }

    get matchPattern(): RegExp {
        return this._matchPattern
    }

    get replacePattern(): ReplacePattern {
        return this._replacePattern
    }

    get requestTypes(): RequestType[] {
        return this._requestTypes
    }

    get description(): string {
        return this._description
    }

    get preprocessors(): Preprocessor[] {
        return this._preprocessors
    }

    get exceptions(): RegExp[] {
        return this._exceptions
    }

    get isActive(): boolean {
        return this._active
    }

    /**
     * Activates the rule.
     */
    public activate() {
        this._active = true
    }

    /**
     * Deactivates the rule.
     */
    public deactivate() {
        this._active = false
    }

    /**
     * Returns this rule as string.
     *
     * @returns the rule as string
     */
    public toString(): string {
        return this._matchPattern.source
    }

    /**
     * Creates a new {@link Rule} object from the given json object.
     *
     * @param value the json object
     * @throws {@link IllegalArgumentException} if the given {@code value} is not valid
     */
    public static fromJSON(): any {
        // FIXME: implement correct serialization

        return undefined
    }
}
