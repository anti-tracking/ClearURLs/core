/*
 * ClearURLs
 * Copyright (c) 2017-2022 Kevin Röbert.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import IllegalArgumentException from '../exceptions/illegalArgumentException'

export default class RequestType {
    private static VALUES: RequestType[] = new Array<RequestType>()
    /**
     * Models all sorts of {@link RequestType}'s
     */
    public static ALL: RequestType = RequestType.of('ALL')
    private readonly _type: string

    /**
     * Creates a new {@link RequestType} object.
     *
     * @param type the type
     * @throws {@link IllegalArgumentException} if the given {@code type} is not valid
     */
    private constructor(type: string) {
        if (!type) {
            throw new IllegalArgumentException('The given `type` can not be empty.')
        }

        this._type = type
    }

    /**
     * Creates a new {@link RequestType} object.
     *
     * @param type the type
     * @throws {@link IllegalArgumentException} if the given {@code type} is not valid
     */
    public static of(type: string): RequestType {
        return new RequestType(type).intern()
    }

    /**
     * Creates a new {@link RequestType} object from the given json object.
     *
     * @param value the json object
     * @throws {@link IllegalArgumentException} if the given {@code value} is not valid
     */
    public static fromJSON(value: object): RequestType {
        const type: RequestType = Object.assign(new RequestType(' '), value)

        if (type._type === ' ' || type._type === '') {
            throw new IllegalArgumentException('The given `value` can not be deserialized to an object ' +
                'of type `RequestType`.')
        }


        return type.intern()
    }

    get type(): string {
        return this._type
    }

    /**
     * This method should ensure that every {@link RequestType} is only hold exactly once in memory.
     * Since there are only a small and limited number of {@link RequestType}'s, it is okay to hold them
     * permanently in memory.
     */
    private intern(): RequestType {
        for (const type of RequestType.VALUES) {
            if (type._type === this._type) {
                return type
            }
        }

        RequestType.VALUES.push(this)

        return this
    }
}
