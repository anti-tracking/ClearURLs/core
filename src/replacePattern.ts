/*
 * ClearURLs
 * Copyright (c) 2017-2022 Kevin Röbert.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

export default class ReplacePattern {
    /**
     * Models a {@link ReplacePattern} that just removes everything
     */
    public static readonly REMOVE: ReplacePattern = new ReplacePattern('')
    private readonly _pattern: string

    constructor(pattern: string) {
        this._pattern = pattern
    }

    get pattern(): string {
        return this._pattern
    }

    /**
     * Does apply the given {@code values} to the saved {@code _pattern} and replaces all occurrences accordingly.
     * <p>
     * E.g. §1§://§2§.clearurls.xyz/ with the given {@code values=['https', 'test']} would result
     * in https://test.clearurls.xyz/
     *
     * @param values the values to apply
     * @return the resulting string that was substituted with the values
     */
    public apply(values: string[]): string {
        // quick path for remove patterns
        if (this._pattern === '') {
            return ''
        }

        return this._pattern.replace(/§\d+?§/g, (placeholder) => {
            const index: number = parseInt(placeholder.substring(1, placeholder.length - 1), 10) - 1
            const rtn: string | undefined = values[index]

            if (typeof rtn !== 'undefined') {
                return rtn
            }

            return ''
        })
    }
}
